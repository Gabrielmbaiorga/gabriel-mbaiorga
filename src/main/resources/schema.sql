DROP TABLE IF EXISTS drone;
CREATE TABLE drone
(
    id            BIGINT AUTO_INCREMENT,
    serial_no     VARCHAR(100) NOT NULL,
    model         VARCHAR(255) NOT NULL,
    weight_limit  DOUBLE CHECK (weight_limit <= 500),
    batt_capacity INTEGER      NOT NULL,
    drone_state   VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (serial_no)
);


DROP TABLE IF EXISTS medication;

CREATE TABLE medication
(
    id       BIGINT AUTO_INCREMENT,
    med_name VARCHAR(255) NOT NULL,
    weight   DOUBLE       NOT NULL,
    code     VARCHAR(255) NOT NULL,
    med_img  BLOB,
    PRIMARY KEY (id),
    UNIQUE (code)
);

DROP TABLE IF EXISTS drone_history;
CREATE TABLE drone_history
(
    id   BIGINT AUTO_INCREMENT,
    drone_id     BIGINT NOT NULL,
    serial_number   VARCHAR(255) NOT NULL,
    battery_capacity INTEGER      NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS drone_medication;
CREATE TABLE drone_medication
(
    id  BIGINT AUTO_INCREMENT,
    drone_id BIGINT NOT NULL ,
    medication_id BIGINT NOT NULL ,
    quantity INT,
    med_weight DOUBLE PRECISION,
    FOREIGN KEY (drone_id) REFERENCES drone(id),
    FOREIGN KEY (medication_id) REFERENCES medication(id)
);

