package com.sdl.gabrielmbaiorga.enums;

import java.util.Arrays;
import java.util.List;

public enum DroneModel {
    LightWeight,
    MiddleWeight,
    CruiserWeight,
    HeavyWeight;

    public static DroneModel droneValueOf( String value){
        try {
            return DroneModel.valueOf(value);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid model input value: " + value);
        }
    }


}