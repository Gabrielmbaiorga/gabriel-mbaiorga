package com.sdl.gabrielmbaiorga.enums;

import java.util.Arrays;
import java.util.List;

public enum DroneState {
  IDLE,
  LOADING,
  LOADED,
  DELIVERING,
  DELIVERED,
  RETURNING

}
