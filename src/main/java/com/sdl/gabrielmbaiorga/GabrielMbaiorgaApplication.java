package com.sdl.gabrielmbaiorga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GabrielMbaiorgaApplication {

    public static void main(String[] args) {
        SpringApplication.run(GabrielMbaiorgaApplication.class, args);
    }

}
