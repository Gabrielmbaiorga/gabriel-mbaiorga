package com.sdl.gabrielmbaiorga.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MedicationNotFoundException extends RuntimeException{
  private static final long serialVersionUID = -5218143265247846948L;

  public MedicationNotFoundException(String message) {
    super(message);
  }
}