package com.sdl.gabrielmbaiorga.dto;

import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.models.Medication;

import lombok.Data;

@Data
public class DroneLoadedItemDto {
private Drone drone;
private Double totalMedWeight;
private Medication medicationItem;

private Integer quantity;

  public DroneLoadedItemDto(Drone drone, Double totalMedWeight, Medication medicationItem,
      Integer quantity) {
    this.drone = drone;
    this.totalMedWeight = totalMedWeight;
    this.medicationItem = medicationItem;
    this.quantity = quantity;
  }

  public DroneLoadedItemDto() {
  }
}
