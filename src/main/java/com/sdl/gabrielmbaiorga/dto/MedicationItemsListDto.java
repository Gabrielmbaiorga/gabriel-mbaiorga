package com.sdl.gabrielmbaiorga.dto;

import java.util.List;
import lombok.Data;

@Data
public class MedicationItemsListDto {
    private DroneDto droneSerialNo;
    private List<MedicationDto> medicationItem;

    public MedicationItemsListDto(DroneDto droneSerialNo, List<MedicationDto> medicationItem) {
        this.droneSerialNo = droneSerialNo;
        this.medicationItem = medicationItem;
    }
}
