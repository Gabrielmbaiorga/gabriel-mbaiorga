package com.sdl.gabrielmbaiorga.dto;

import jakarta.persistence.Column;
import lombok.Data;

@Data
public class MedicationDto {
  private String name;
  private Double weight;
  private String code;
  private byte[] medImg;
}




