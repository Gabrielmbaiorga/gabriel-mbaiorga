package com.sdl.gabrielmbaiorga.dto;

import lombok.Data;

import java.util.List;

@Data
public class MedicationItemsDto {
    private String droneSerialNo;
    private String  medCode;
    private Integer quantity;

}


