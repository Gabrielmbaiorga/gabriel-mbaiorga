package com.sdl.gabrielmbaiorga.dto;

import com.sdl.gabrielmbaiorga.enums.DroneModel;
import com.sdl.gabrielmbaiorga.enums.DroneState;
import java.io.Serializable;
import lombok.Data;

@Data
public class DroneDto implements Serializable {

  private String serialNumber;
  private DroneModel model;
  private Double weightLimit;
  private Integer batteryCapacity;
  private DroneState state;
}
