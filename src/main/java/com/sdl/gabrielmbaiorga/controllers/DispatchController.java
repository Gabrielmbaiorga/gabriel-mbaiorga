package com.sdl.gabrielmbaiorga.controllers;

import com.sdl.gabrielmbaiorga.api.Response;
import com.sdl.gabrielmbaiorga.dto.DroneDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsDto;
import com.sdl.gabrielmbaiorga.exception.BadRequestException;
import com.sdl.gabrielmbaiorga.exception.ConflictRequestException;
import com.sdl.gabrielmbaiorga.exception.DroneNotFoundException;
import com.sdl.gabrielmbaiorga.service.DroneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/app")
public class DispatchController {
    private final Logger logger = LoggerFactory.getLogger(DispatchController.class);

    private final DroneService droneService;

    public DispatchController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping("/register")
    public ResponseEntity<Response> registerDrone(@RequestBody DroneDto droneDto) {
        return new ResponseEntity<>(droneService.registerDrone(droneDto), HttpStatus.OK);
    }

    @PostMapping("/load-medication")
    public ResponseEntity<Response> loadDroneWtMedicationItems(@RequestBody MedicationItemsDto items) {
        return new ResponseEntity<>(droneService.loadMedicationItems(items), HttpStatus.OK);
    }

    @GetMapping("/medications/{serialNo}")
    public ResponseEntity<Response> getLoadedDroneMedItems(@PathVariable String serialNo) {
        return new ResponseEntity<>(droneService.loadedDroneMedItems(serialNo), HttpStatus.OK);
    }


    @GetMapping("/battery-level/{serialNo}")
    public ResponseEntity<Response> getDroneBatteryLevel(@PathVariable String serialNo) {
        return new ResponseEntity<>(droneService.droneBattLevel(serialNo), HttpStatus.OK);
    }

    @GetMapping("/drones")
    public ResponseEntity<Response> getAvailableDrones() {
        return new ResponseEntity<>(droneService.availableDrones(), HttpStatus.OK);
    }
}
