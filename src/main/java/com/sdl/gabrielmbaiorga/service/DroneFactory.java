package com.sdl.gabrielmbaiorga.service;


public interface DroneFactory<T, R> {
  T create(R r);
}

