package com.sdl.gabrielmbaiorga.service;

import com.sdl.gabrielmbaiorga.api.Response;
import com.sdl.gabrielmbaiorga.dto.DroneDto;
import com.sdl.gabrielmbaiorga.dto.MedicationDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsDto;
import java.util.List;

public interface DroneService {
  Response registerDrone(DroneDto dto);
  Response loadMedicationItems(MedicationItemsDto dto);

  Response loadedDroneMedItems(String dsn);

  Response availableDrones();

  Response droneBattLevel(String dsn);
}
