package com.sdl.gabrielmbaiorga.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DroneEventHandler implements ApplicationListener<DronePropertyChangeEvent> {

  private Logger logger = LoggerFactory.getLogger(DroneEventHandler.class);


  @Override
  public void onApplicationEvent(DronePropertyChangeEvent event) {


    logger.info("Drone consumable event {}", event.getSource());
  }
}
