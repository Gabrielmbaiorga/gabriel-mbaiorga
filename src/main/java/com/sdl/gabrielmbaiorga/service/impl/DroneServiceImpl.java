package com.sdl.gabrielmbaiorga.service.impl;

import com.sdl.gabrielmbaiorga.api.Response;
import com.sdl.gabrielmbaiorga.dto.DroneDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsListDto;
import com.sdl.gabrielmbaiorga.enums.DroneState;
import com.sdl.gabrielmbaiorga.exception.BadRequestException;
import com.sdl.gabrielmbaiorga.exception.ConflictRequestException;
import com.sdl.gabrielmbaiorga.exception.DroneNotFoundException;
import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.repository.DroneMedItemRepository;
import com.sdl.gabrielmbaiorga.repository.DroneRepository;
import com.sdl.gabrielmbaiorga.repository.MedicationRepository;
import com.sdl.gabrielmbaiorga.service.DroneFactory;
import com.sdl.gabrielmbaiorga.service.DroneService;
import com.sdl.gabrielmbaiorga.service.DroneValidator;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DroneServiceImpl  extends  UtilityServiceImpl implements DroneService {


  private Logger logger = LoggerFactory.getLogger(DroneServiceImpl.class);
  private final DroneEventPublisher eventPublisher;
  private final DroneRepository droneRepository;
  private final DroneMedItemRepository medItemRepository;
  private final MedicationRepository medicationRepository;
  private final DroneValidator<DroneDto> validator;
  private final DroneFactory<Drone, DroneDto> factory;

  private final ModelMapper modelMapper;



  @Autowired
  public DroneServiceImpl(DroneEventPublisher eventPublisher, DroneRepository droneRepository,
      DroneMedItemRepository medItemRepository, MedicationRepository medicationRepository,
      DroneValidator<DroneDto> validator, DroneFactory<Drone, DroneDto> factory,
      ModelMapper modelMapper) {
    super(droneRepository, eventPublisher, medItemRepository, medicationRepository, modelMapper);
    this.eventPublisher = eventPublisher;
    this.droneRepository = droneRepository;
    this.medItemRepository = medItemRepository;
    this.medicationRepository = medicationRepository;
    this.validator = validator;
    this.factory = factory;
    this.modelMapper = modelMapper;
  }


  @Override
  public Response registerDrone(DroneDto dto) {
    validator.validate(dto);
    Drone drone = factory.create(dto);
    DroneDto savedDrone = modelMapper.map(droneRepository.save(drone), DroneDto.class);
    eventPublisher.publish(new DronePropertyChangeEvent(savedDrone));
    return new Response(HttpStatus.CREATED, "Drone Registered Successfully", savedDrone);
  }


  @Override
  public Response loadMedicationItems(MedicationItemsDto dto) {
    Drone dn = droneRepository.getDroneBySerialNumber(dto.getDroneSerialNo())
        .orElseThrow(() -> new DroneNotFoundException("Drone not exists"));

    try {
      MedicationItemsListDto listDto = addMedication(dn,
          dto);
      return new Response(HttpStatus.OK, "Medication added successfuly", listDto);
    } catch (ConflictRequestException e) {
    return new Response(HttpStatus.CONFLICT,"Item already exist" , e.getMessage());
  } catch (BadRequestException e) {
      return new Response(HttpStatus.BAD_REQUEST, "Failed to load Medication Item", e.getMessage());
    }catch (Exception e) {
      return new Response(HttpStatus.BAD_REQUEST, "Failed to load Medication Item", e.getMessage());
    }

  }


  @Override
  public Response loadedDroneMedItems(String dsn) {
    try {
      return new Response(HttpStatus.OK, "list of drones",null );
    } catch (BadRequestException e) {
      return new Response(HttpStatus.BAD_REQUEST, "Request failed", e.getMessage());
    }
  }


  @Override
  public Response availableDrones() {
    try {
      List<DroneDto> availableDrones = getAvailableDrones();
      return new Response(HttpStatus.OK, "Below the list of available drones", availableDrones);
    } catch (DroneNotFoundException e) {
      return new Response(HttpStatus.NOT_FOUND, "Available drones fail ", e.getMessage());
    }
  }

  @Override
  public Response droneBattLevel(java.lang.String dsn) {
    try {
      Integer batteryCapacity = getDroneBatteryCapacity(dsn);
      return new Response(HttpStatus.OK, "Drone Battery Capacity ", batteryCapacity + " %");
    } catch (DroneNotFoundException e) {
      return new Response(HttpStatus.NOT_FOUND, "Drone not exists", e.getMessage());
    } catch (BadRequestException e) {
      return new Response(HttpStatus.BAD_REQUEST, "Bad request", e.getMessage());
    }
  }


  private List<DroneDto> getAvailableDrones() throws DroneNotFoundException {
    List<Drone> drones = droneRepository.getDroneByDroneState(DroneState.IDLE);
    if (drones.isEmpty()) {
      throw new DroneNotFoundException("Drone not found");
    }
    return mapDronesToDtos(drones);
  }

  private List<DroneDto> mapDronesToDtos(List<Drone> drones) {
    return drones.stream()
        .map(drone -> modelMapper.map(drone, DroneDto.class))
        .collect(Collectors.toList());
  }
}
