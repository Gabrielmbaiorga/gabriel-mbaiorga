package com.sdl.gabrielmbaiorga.service.impl;

import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.service.DroneInformationReceived;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class DroneInformationPublisherService {

  private List<DroneInformationReceived> subscribers;

  public DroneInformationPublisherService() {
    subscribers = new ArrayList<>();
  }


  public void subscribe(DroneInformationReceived subscriber) {
    subscribers.add(subscriber);
  }


  public void unsubscribe(DroneInformationReceived subscriber) {
    subscribers.remove(subscriber);
  }

  public void publish(List<Drone> data) {
    for (DroneInformationReceived subscriber : subscribers) {
      subscriber.receivedDroneInformation(data);
    }
  }

}
