package com.sdl.gabrielmbaiorga.service.impl;


import static com.sdl.gabrielmbaiorga.api.DroneConstants.MAX_DRONE_BATTERY_LIFE;
import static com.sdl.gabrielmbaiorga.api.DroneConstants.MAX_DRONE_WEIGHT_LIMIT;

import com.sdl.gabrielmbaiorga.dto.DroneDto;
import com.sdl.gabrielmbaiorga.service.DroneValidator;
import org.springframework.stereotype.Component;

@Component
class DroneValidatorImpl implements DroneValidator<DroneDto> {

  @Override
  public void validate(DroneDto dto) {
    boolean isValidWeightLimit = dto.getWeightLimit() <= MAX_DRONE_WEIGHT_LIMIT;
    boolean isValidDroneCap = dto.getBatteryCapacity() <= MAX_DRONE_BATTERY_LIFE;
    if (!isValidWeightLimit) {
      throw new IllegalArgumentException("Drone Weight Exceeded !");
    }
    if (!isValidDroneCap) {
      throw new IllegalArgumentException("Drone Battery Percent exceeded ! !");
    }
  }
}
