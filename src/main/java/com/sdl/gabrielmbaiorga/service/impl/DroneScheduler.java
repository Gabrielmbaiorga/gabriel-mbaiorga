package com.sdl.gabrielmbaiorga.service.impl;

import com.sdl.gabrielmbaiorga.api.DroneConstants;
import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.models.DroneHistory;
import com.sdl.gabrielmbaiorga.repository.DroneHistoryRepository;
import com.sdl.gabrielmbaiorga.repository.DroneRepository;

import java.text.MessageFormat;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@Configuration
public class DroneScheduler {
  private Logger logger = LoggerFactory.getLogger(DroneScheduler.class);

  private final DroneHistoryRepository historyRepository;
  private final DroneRepository droneRepository;

  public DroneScheduler(DroneHistoryRepository historyRepository, DroneRepository droneRepository) {
    this.historyRepository = historyRepository;
    this.droneRepository = droneRepository;
  }

  @Scheduled(fixedRate = DroneConstants.DRONE_SCHEDULE_RATE)
  public void checkDronesBatteryLevels() {
    List<Drone> drones = droneRepository.findAll();
    if (drones.isEmpty()) return;

    for (Drone drone : drones) {
      historyRepository.save(new DroneHistory(drone.getId(), drone.getSerialNumber(), drone.getBatteryCapacity()));
      logger.info(MessageFormat.format("Drone with serial number {0} battery capacity {1}%", drone.getSerialNumber(), drone.getBatteryCapacity()));
    }
  }
}
