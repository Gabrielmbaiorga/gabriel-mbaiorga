package com.sdl.gabrielmbaiorga.service.impl;

import com.sdl.gabrielmbaiorga.dto.DroneDto;
import com.sdl.gabrielmbaiorga.enums.DroneModel;
import com.sdl.gabrielmbaiorga.enums.DroneState;
import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.service.DroneFactory;
import org.springframework.stereotype.Component;

@Component
public class DroneFactorImpl implements DroneFactory<Drone, DroneDto> {
  @Override
  public Drone create(DroneDto dto) {
    DroneModel.droneValueOf(dto.getModel().name());
    Drone drone = new Drone();
    drone.setSerialNumber(dto.getSerialNumber());
    drone.setModel(dto.getModel());
    drone.setWeightLimit(dto.getWeightLimit());
    drone.setBatteryCapacity(dto.getBatteryCapacity());
    drone.setDroneState(DroneState.IDLE);
    return drone;
  }
}

