package com.sdl.gabrielmbaiorga.service.impl;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

@Component
public class DroneEventPublisher implements ApplicationEventPublisherAware {
  private ApplicationEventPublisher publisher;


  @Override
  public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
    this.publisher = applicationEventPublisher;
  }

  public void publish(DronePropertyChangeEvent changeEvent){
    this.publisher.publishEvent(changeEvent);
  }
}
