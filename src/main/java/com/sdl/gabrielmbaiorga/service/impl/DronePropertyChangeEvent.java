package com.sdl.gabrielmbaiorga.service.impl;

import org.springframework.context.ApplicationEvent;

public class DronePropertyChangeEvent extends ApplicationEvent {

  public DronePropertyChangeEvent(Object source) {
    super(source);
  }
}
