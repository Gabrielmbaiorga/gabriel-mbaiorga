package com.sdl.gabrielmbaiorga.service.impl;

import static com.sdl.gabrielmbaiorga.api.DroneConstants.MINIMUM_BATTERY_LEVEL;

import com.sdl.gabrielmbaiorga.dto.DroneDto;
import com.sdl.gabrielmbaiorga.dto.DroneLoadedItemDto;
import com.sdl.gabrielmbaiorga.dto.MedicationDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsListDto;
import com.sdl.gabrielmbaiorga.enums.DroneState;
import com.sdl.gabrielmbaiorga.exception.DroneNotFoundException;
import com.sdl.gabrielmbaiorga.exception.MedicationNotFoundException;
import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.models.DroneMedicationItems;
import com.sdl.gabrielmbaiorga.models.Medication;
import com.sdl.gabrielmbaiorga.repository.DroneMedItemRepository;
import com.sdl.gabrielmbaiorga.repository.DroneRepository;
import com.sdl.gabrielmbaiorga.repository.MedicationRepository;
import com.sdl.gabrielmbaiorga.service.UtilityService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class UtilityServiceImpl implements UtilityService {

  private Logger logger = LoggerFactory.getLogger(UtilityServiceImpl.class);
  private final DroneRepository droneRepository;
  private final DroneEventPublisher eventPublisher;
  private final DroneMedItemRepository medItemRepository;

  private final MedicationRepository medicationRepository;
  private boolean isLoadable = true;
  private final ModelMapper modelMapper;

  @Autowired
  public UtilityServiceImpl(DroneRepository droneRepository, DroneEventPublisher eventPublisher,
      DroneMedItemRepository medItemRepository, MedicationRepository medicationRepository,
      ModelMapper modelMapper) {
    this.droneRepository = droneRepository;
    this.eventPublisher = eventPublisher;
    this.medItemRepository = medItemRepository;
    this.medicationRepository = medicationRepository;
    this.modelMapper = modelMapper;
  }

  @Override
  public DroneLoadedItemDto addMedicationAndUpdateWeight(Drone dn, MedicationItemsDto medicationItem) {
    Medication medication = medicationRepository.getMedicationByCode(medicationItem.getMedCode());
    if (medication == null) {
      throw new MedicationNotFoundException("Medication Not Found");
    }

    double droneWeightLimit = dn.getWeightLimit();
    Optional<DroneMedicationItems> items = medItemRepository.getDroneMedicationItemsByDrone(dn);
    double totalMedWeight = items.map(i -> i.getTotalMedWeight()).orElse(0.0);

    if (totalMedWeight + medication.getWeight() > droneWeightLimit) {
      throw new IllegalArgumentException("Total medication weight exceeds drone weight limit");
    }

    DroneLoadedItemDto droneLoadedItemDto = new DroneLoadedItemDto(dn, totalMedWeight + medication.getWeight(), medication, medicationItem.getQuantity());
    eventPublisher.publish(new DronePropertyChangeEvent(droneLoadedItemDto));

    updateDroneState(dn, totalMedWeight + medication.getWeight() == droneWeightLimit ? DroneState.LOADED : DroneState.LOADING);
    return droneLoadedItemDto;
  }


  @Override
  public MedicationItemsListDto addMedication(Drone dn, MedicationItemsDto medicationItem){
    try {
      Integer battLevel = getDroneBatteryCapacity(dn.getSerialNumber());
      if (battLevel < MINIMUM_BATTERY_LEVEL) {
        throw new Exception("Drone can not load when the battery is below 25 %");
      }
      DroneLoadedItemDto droneLodItemDto = addMedicationAndUpdateWeight(dn, medicationItem);
      DroneMedicationItems medItem = new DroneMedicationItems(droneLodItemDto.getDrone(),
          droneLodItemDto.getMedicationItem(),
          droneLodItemDto.getQuantity(),
          droneLodItemDto.getTotalMedWeight());
      DroneMedicationItems save = medItemRepository.saveAndFlush(medItem);
      logger.info("the saved item is {}", save);
      return toDtos(save);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  private MedicationItemsListDto toDtos(DroneMedicationItems save) {
    DroneDto droneDto = modelMapper.map(save.getDrone(), DroneDto.class);
    droneDto.setWeightLimit(save.getTotalMedWeight());

    List<MedicationDto> medicationDtos = save.getMedication().getDroneMedications().stream()
        .map(md -> modelMapper.map(md.getMedication(), MedicationDto.class))
        .collect(Collectors.toList());

    return new MedicationItemsListDto(droneDto, medicationDtos);
  }


  @Override
  public Double getDroneWeightLimit(java.lang.String dsn) {
    return droneRepository.getDroneBySerialNumber(dsn)
        .orElseThrow(() -> new DroneNotFoundException("Drone not exists"))
        .getWeightLimit();
  }

  @Override
  public boolean isDroneLoadable() {
    return false;
  }

  @Override
  public Drone updateDroneState(Drone dn, DroneState state) {
    dn.setDroneState(state);
    Drone drone = droneRepository.save(dn);
    return drone;
  }


  @Override
  public Integer getDroneBatteryCapacity(java.lang.String dsn) throws DroneNotFoundException {
    return droneRepository.getDroneBySerialNumber(dsn)
        .orElseThrow(() -> new DroneNotFoundException("Drone not exists"))
        .getBatteryCapacity();
  }

  @Override
  public Double updateTotalMedWeight(Double tmw, Drone dn, Double weight) {
    Optional<DroneMedicationItems> itemRepositoryReferenceById = medItemRepository.getDroneMedicationItemsByDrone(dn);
    itemRepositoryReferenceById.get().setTotalMedWeight(tmw + weight);
    DroneMedicationItems wt = medItemRepository.saveAndFlush(itemRepositoryReferenceById.get());
    return wt.getTotalMedWeight();
  }

}
