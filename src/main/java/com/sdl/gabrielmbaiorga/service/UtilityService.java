package com.sdl.gabrielmbaiorga.service;

import com.sdl.gabrielmbaiorga.dto.DroneLoadedItemDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsDto;
import com.sdl.gabrielmbaiorga.dto.MedicationItemsListDto;
import com.sdl.gabrielmbaiorga.enums.DroneState;
import com.sdl.gabrielmbaiorga.exception.DroneNotFoundException;
import com.sdl.gabrielmbaiorga.models.Drone;

public interface UtilityService {

  DroneLoadedItemDto addMedicationAndUpdateWeight(Drone dn, MedicationItemsDto medicationItem);

  MedicationItemsListDto addMedication(Drone dn, MedicationItemsDto medicationItems)
      throws Exception;

  Double getDroneWeightLimit(java.lang.String dsn);

  boolean isDroneLoadable();

  Drone updateDroneState(Drone dn, DroneState state);

  Integer getDroneBatteryCapacity(java.lang.String dsn) throws DroneNotFoundException;

  Double updateTotalMedWeight(Double totalMedWeight,Drone dn, Double weight);
}
