package com.sdl.gabrielmbaiorga.service;

import com.sdl.gabrielmbaiorga.models.Drone;
import java.util.List;

public interface DroneInformationReceived {
  void receivedDroneInformation(List<Drone> data);
}
