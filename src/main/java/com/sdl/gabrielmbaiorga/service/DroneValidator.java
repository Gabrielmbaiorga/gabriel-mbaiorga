package com.sdl.gabrielmbaiorga.service;

public interface DroneValidator<T> {
  void validate(T t);
}
