package com.sdl.gabrielmbaiorga.models;

import com.sdl.gabrielmbaiorga.enums.DroneModel;
import com.sdl.gabrielmbaiorga.enums.DroneState;
import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.NotBlank;
import java.util.List;
import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "drone")
public class Drone extends BaseEntity{

    @Length(max = 100, message = "Serial Number length must be 100 characters")
    @Column(name = "serial_no", length = 100, unique = true)
    @NotBlank(message = "Serial number must not be blank")
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private DroneModel model;
    @DecimalMax(value = "500", message = "Weight limit is 500gr max")
    @Column(name = "weight_limit")
    private Double weightLimit;

    @Column(name = "batt_capacity")
    private Integer batteryCapacity;
    @Enumerated(EnumType.STRING)
    private DroneState droneState;

    @OneToMany(mappedBy = "drone")
    private List<DroneMedicationItems> droneMedications;


    public Drone() {
    }


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public DroneModel getModel() {
        return model;
    }

    public void setModel(DroneModel model) {
        this.model = model;
    }

    public Double getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(Double weightLimit) {
        this.weightLimit = weightLimit;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public DroneState getDroneState() {
        return droneState;
    }

    public void setDroneState(DroneState droneState) {
        this.droneState = droneState;
    }

    public List<DroneMedicationItems> getDroneMedications() {
        return droneMedications;
    }

    public void setDroneMedications(
        List<DroneMedicationItems> droneMedications) {
        this.droneMedications = droneMedications;
    }

    @Override
    public String toString() {
        return "Drone{" +
            "serialNumber='" + serialNumber + '\'' +
            ", model=" + model +
            ", weightLimit=" + weightLimit +
            ", batteryCapacity=" + batteryCapacity +
            ", droneState=" + droneState +
            ", droneMedications=" + droneMedications +
            '}';
    }
}
