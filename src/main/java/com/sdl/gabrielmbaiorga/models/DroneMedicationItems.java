package com.sdl.gabrielmbaiorga.models;

import jakarta.persistence.*;


@Entity
@Table(name = "drone_medication")
public class DroneMedicationItems extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "drone_id",referencedColumnName = "id")
    private Drone drone;

    @ManyToOne
    @JoinColumn(name = "medication_id", referencedColumnName = "id")
    private Medication medication;

    private Integer quantity;

    @Column(name = "med_weight")
    private Double totalMedWeight;

    public DroneMedicationItems() {
    }

    public DroneMedicationItems(Drone drone, Medication medication, Integer quantity,
        Double totalMedWeight) {
        this.drone = drone;
        this.medication = medication;
        this.quantity = quantity;
        this.totalMedWeight = totalMedWeight;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotalMedWeight() {
        return totalMedWeight;
    }

    public void setTotalMedWeight(Double totalMedWeight) {
        this.totalMedWeight = totalMedWeight;
    }

}
