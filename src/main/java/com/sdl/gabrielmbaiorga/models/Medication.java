package com.sdl.gabrielmbaiorga.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "medication")
public class Medication extends BaseEntity{

    @Pattern(regexp = "^[-a-zA-Z_\\d]*$", message = "medication not valid")
    @Column(name = "med_name")
    private String name;
    private Double weight;
    @Column(unique = true)
    private String code;
    @Column(columnDefinition = "BLOB")
    private byte[] medImg;

    @OneToMany(mappedBy = "medication")
    private List<DroneMedicationItems> droneMedications;


    public Medication() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public byte[] getMedImg() {
        return medImg;
    }

    public void setMedImg(byte[] medImg) {
        this.medImg = medImg;
    }

    public List<DroneMedicationItems> getDroneMedications() {
        return droneMedications;
    }

    public void setDroneMedications(
        List<DroneMedicationItems> droneMedications) {
        this.droneMedications = droneMedications;
    }

    @Override
    public String toString() {
        return "Medication{" +
            "name='" + name + '\'' +
            ", weight=" + weight +
            ", code='" + code + '\'' +
            ", medImg=" + Arrays.toString(medImg) +
            ", droneMedications=" + droneMedications +
            '}';
    }
}
