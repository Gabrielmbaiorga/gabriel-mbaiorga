package com.sdl.gabrielmbaiorga.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "drone_history")
public class DroneHistory extends BaseEntity {

    @Column(name = "drone_id")
    private Long droneId;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "battery_capacity")
    private Integer batteryCapacity;

    public DroneHistory() {
    }

    public DroneHistory(Long droneId, String serialNumber, Integer batteryCapacity) {
        this.droneId = droneId;
        this.serialNumber = serialNumber;
        this.batteryCapacity = batteryCapacity;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Long getDroneId() {
        return droneId;
    }

    public void setDroneId(Long droneId) {
        this.droneId = droneId;
    }
}
