package com.sdl.gabrielmbaiorga.api;

public class DroneConstants {

  public static final int MAX_DRONE_WEIGHT_LIMIT = 500;
  public static final int MAX_DRONE_BATTERY_LIFE = 100;
  public static final long DRONE_SCHEDULE_RATE = 10000;
  public static final long MINIMUM_BATTERY_LEVEL = 25;
  public static final String DATE_PATTERN = "yyyy-MM-dd hh:mm:ss";

  public static final String PATTERN = "^[A-Z0-9_]+$";
  public static final String NUMBERS_PATTERN = "^[a-zA-Z0-9_-]+$";

  public static int globalWeightCounter = 0;

  public void updateWeightCounter(){
    globalWeightCounter++;
  }

  public int getGlobalWeightCounter(){
    return globalWeightCounter;
  }
   public void resetCounter(){
    globalWeightCounter = 0;
   }
}
