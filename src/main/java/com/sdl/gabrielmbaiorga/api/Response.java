package com.sdl.gabrielmbaiorga.api;

import lombok.Data;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotNull;

@Data
@NotNull
public class Response {

  private HttpStatus statusCode;
  private String message;
  private Object data;

  public Response(HttpStatus statusCode, String message, Object data) {
    this.statusCode = statusCode;
    this.message = message;
    this.data = data;
  }

  @Override
  public String toString() {
    return "Drone Response{" +
        "status Code=" + statusCode +
        ", message='" + message + '\'' +
        ", data=" + data +
        '}';
  }
}
