package com.sdl.gabrielmbaiorga.repository;

import com.sdl.gabrielmbaiorga.models.DroneHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneHistoryRepository extends JpaRepository<DroneHistory, Long> {
}
