package com.sdl.gabrielmbaiorga.repository;

import com.sdl.gabrielmbaiorga.models.Drone;
import com.sdl.gabrielmbaiorga.models.DroneMedicationItems;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneMedItemRepository extends JpaRepository<DroneMedicationItems, Long> {


  Optional<DroneMedicationItems> getDroneMedicationItemsByDrone(Drone aLong);

}
