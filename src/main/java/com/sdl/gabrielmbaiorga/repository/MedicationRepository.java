package com.sdl.gabrielmbaiorga.repository;


import com.sdl.gabrielmbaiorga.models.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

  Medication getMedicationByCode(String medCode);

}
