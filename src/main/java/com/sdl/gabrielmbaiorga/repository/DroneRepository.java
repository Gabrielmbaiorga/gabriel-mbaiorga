package com.sdl.gabrielmbaiorga.repository;


import com.sdl.gabrielmbaiorga.enums.DroneState;
import com.sdl.gabrielmbaiorga.models.Drone;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

  List<Drone> getDroneByDroneState(DroneState drone_state);

  Optional<Drone> getDroneBySerialNumber(java.lang.String dsn);
}
