## Drones

[[_TOC_]]

---

:scroll: **START**

### Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**.
Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the
drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

---

### Application setup

##### To setup this application, you'll need the following:
- Java 17
- H2 database (InMemory Database)
- Maven
- Postman
---

### Build Instructions

Before  Building this project **please make sure you have java 8 or above and Maven installed on your machine**:

- Clone the repository to your local machine;
- Navigate to the root directory of the project in the command line;
- Run the command **mvn clean install** to build the project;

---

### Run Instructions

- After building the project, navigate to the target directory in the command line;
- Run the command **java -jar project-name.jar** to start the application;

---
### Test Instructions

- Make sure the application, Navigate to postman and import the collection from the 'resources' directory;
- Test all endpoint using the collection in postman;
- Make sure the application is running before running the tests;
- Navigate to the root directory of the project in the command line;
- Run the command mvn test to execute the test cases;

:scroll: **END**
